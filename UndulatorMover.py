import sys
import math
import numpy as np
from epics import PV,caput
from threading import Thread
import datetime
import time

from PyQt5.QtCore import QObject,pyqtSignal

class UndulatorMover(QObject):

    sigmover = pyqtSignal(int)  # signal for mover status
    def __init__(self): 
        QObject.__init__(self)
        self.tolerance=0.01
        self.timeout=5.0 
        self.timereset = 2.0

        self.state = 0  # 0 = idle, 1 = moving, 2 = timeout, 3 = abort, 4 = out of range
        self.doAbort = False

    
    def abort(self):
        self.doAbort = True

    def outofrange(self):
        self.state=4
        self.sigmover.emit(self.state)

    def move(self,pvsv,setval,pvrb):
        self.pvsv = pvsv
        self.setval=setval
        self.pvrb=pvrb
        Thread(target=self.moving).start()

    def setValues(self):
        for i,pv in enumerate(self.pvsv):
            print(pv.pvname,':',self.setval[i])
            if ':GM-' in pv.pvname:   # undulator mover
                pvon = pv.pvname.split(':')[0]+':GM-ONOFF'
                caput(pvon,1)
                time.sleep(0.1)
                pv.put(self.setval[i])
                pvgo = pv.pvname.split(':')[0]+':GM-GO'
                caput(pvgo,1)
                time.sleep(0.1)                
            else:                   # quadrupole mover
                pv.put(self.setval[i])
                time.sleep(0.01)
                if 'SAT' in pv.pvname:  # this fancy flag exists only for Athos
                    pvmove=pv.pvname.split(':')[0]+':MOVE-XY'
                    caput(pvmove,0)
                    time.sleep(0.5)
                    caput(pvmove,1)


    def moving(self):
        self.state = 1
        self.sigmover.emit(self.state)

        self.setValues()
        # do some stuff for undulator with turn motor on and go action
        # writing the setvalues


        # starting the loop to check for readback
        tstart=datetime.datetime.now()
        treset=datetime.datetime.now()
        self.doAbort = False
        diff = 3
        while(True):
            if self.doAbort:    # check for external abort
                self.state=3
                self.sigmover.emit(self.state)
                return
            dt =datetime.datetime.now()-tstart
            if dt.total_seconds() > self.timeout:  # stop if time is larger than timeout
                self.state=2
                self.sigmover.emit(self.state)
                return
            dt =datetime.datetime.now()-treset
            if dt.total_seconds() > self.timereset:  # apply setvalues again if timeis larger than timereset
                self.setValues()
                treset=datetime.datetime.now()

            rbval = np.array([pv.get() for pv in self.pvrb])
            diff = np.max((rbval-self.setval)**2)
            if diff < self.tolerance:
                self.state=0
                self.sigmover.emit(self.state)
                return    
            time.sleep(0.2)
            self.sigmover.emit(self.state)
