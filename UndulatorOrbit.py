import sys
import datetime
import time
import subprocess

from threading import Thread

import numpy as np
import math
import webbrowser

from PyQt5 import QtGui,QtCore,QtWidgets
from PyQt5.uic import loadUiType
from PyQt5.QtCore import QObject,pyqtSignal

from matplotlib.figure import Figure
from matplotlib import gridspec
import matplotlib.patches as patches
import matplotlib.pyplot as plt

from matplotlib.backends.backend_qt4agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)


sys.path.append('/sf/bd/packages/SFBD/src') # temporary solution till SFBD is an official package
from sfbd.interface.snapshot import snapshot


Ui_UndulatorOrbitGUI, QMainWindow = loadUiType('UndulatorOrbitGUI.ui')

import UndulatorOrbitCore as core
import UndulatorMover as mover

class UndulatorOrbit(QMainWindow,Ui_UndulatorOrbitGUI):

    sigsnap = pyqtSignal(bool)  # signal after thread has been saved

    def __init__(self,):
        super(UndulatorOrbit,self).__init__()

        self.version='4.0.1'

        self.setupUi(self)

        self.snap=snapshot('/sf/data/applications/BD-UndulatorOrbit/snap/SFBD_UndOrbit.req')
        self.snappipe=None
        self.snapsaved=[]
        self.sigsnap.connect(self.updateSnapList)

        self.mover=mover.UndulatorMover()        
        self.mover.sigmover.connect(self.updateMoverStatus)
        self.UIAbort.clicked.connect(self.mover.abort)

        self.BL=[core.UndulatorOrbitCore(True),core.UndulatorOrbitCore(False)]

#        pvs = self.BL[0].getPVChannels()+self.BL[1].getPVChannels()
#        f = open('SFBD_UndOrbit.req','w')
#        for pv in pvs:
#            f.write('%s\n' % pv.pvname)
#        f.close()

        self.updateTable()
        self.UIAramisBPM.itemChanged.connect(self.filterPV)
        self.UIAramisQuad.itemChanged.connect(self.filterPV)
        self.UIAramisUnd.itemChanged.connect(self.filterPV)
        self.UIAthosBPM.itemChanged.connect(self.filterPV)
        self.UIAthosQuad.itemChanged.connect(self.filterPV)
        self.UIAthosUnd.itemChanged.connect(self.filterPV)
        self.UITimeout.editingFinished.connect(self.setTimeout)
        self.UITimeReset.editingFinished.connect(self.setTimeout)
        self.UIMotorTolerance.editingFinished.connect(self.setTimeout)
        self.setTimeout()

        self.UIRange=QtWidgets.QButtonGroup()
        self.UIRange.addButton(self.UIModRange1,0)
        self.UIRange.addButton(self.UIModRange2,1)
        self.UIRange.addButton(self.UIModRange3,2)
        self.UIRange.addButton(self.UIModRange4,3)
        self.UIModRange1.setStyleSheet("QPushButton::checked{background-color : rgb(8,255,8);}")
        self.UIModRange2.setStyleSheet("QPushButton::checked{background-color : rgb(8,255,8);}")
        self.UIModRange3.setStyleSheet("QPushButton::checked{background-color : rgb(8,255,8);}")
        self.UIModRange4.setStyleSheet("QPushButton::checked{background-color : rgb(8,255,8);}")

        self.UITarget=QtWidgets.QButtonGroup()
        self.UITarget.addButton(self.UITarget1,0)
        self.UITarget.addButton(self.UITarget2,1)
        self.UITarget1.setStyleSheet("QPushButton::checked{background-color : rgb(8,255,8);}")
        self.UITarget2.setStyleSheet("QPushButton::checked{background-color : rgb(8,255,8);}")

        
        self.initmpl()
        self.plotrefBPM=None
        self.plotrefQuad=None
        self.plotrefUnd=None
        self.UIRefreshPlot.clicked.connect(self.enforcePlot)

        self.UIBeamline.currentIndexChanged.connect(self.initBeamline)
        self.UIRange.buttonClicked.connect(self.selectRange)
        # initialize all other widgets
        self.initBeamline()


        # basic eventhandler function for changing the orbit.
        self.OffDecX.clicked.connect(self.modify)
        self.OffDecY.clicked.connect(self.modify)
        self.SlpDecX.clicked.connect(self.modify)
        self.SlpDecY.clicked.connect(self.modify)
        self.ParDecX.clicked.connect(self.modify)
        self.ParDecY.clicked.connect(self.modify)
        self.OffIncX.clicked.connect(self.modify)
        self.OffIncY.clicked.connect(self.modify)
        self.SlpIncX.clicked.connect(self.modify)
        self.SlpIncY.clicked.connect(self.modify)
        self.ParIncX.clicked.connect(self.modify)
        self.ParIncY.clicked.connect(self.modify)
        
        self.UICaptureX.clicked.connect(self.capture)
        self.UICaptureY.clicked.connect(self.capture)

        self.UISaveSnap.clicked.connect(self.savesnap)
        self.UILaunchSnap.clicked.connect(self.launchsnap)
        self.UIRestoreSnap.clicked.connect(self.restoresnap)

        # menue items
        self.actionWiki.triggered.connect(self.openWiki)
        self.actionGit.triggered.connect(self.openGit)
        self.UIHelp.clicked.connect(self.openGit)
        self.actionAbout.triggered.connect(self.about)

        # start timer to read orbit and define event handler to change acquisition rate
        self.timer=QtCore.QTimer(self)
        self.timer.timeout.connect(self.plot)
        self.timer.start(1000)
        self.UIAcRate.setCurrentIndex(1)
        self.UIAcRate.currentIndexChanged.connect(self.ToggleTimer)



        # generate initial snap file
        self.snapcomment='Program Launch: '
        self.savesnap()

    def about(self):
        QtWidgets.QMessageBox.about(self,"Undulator Orbit Tool",
                "Version:%s\nContact: Sven Reiche\nEmail: sven.reiche@psi.ch" % self.version)

    def openWiki(self):
        webbrowser.open('https://acceleratorwiki.psi.ch/wiki/Undulator_Pointing_Anpassung')

    def openGit(self):
        webbrowser.open('https://gitlab.psi.ch/reiche/undulatororbit/-/tree/dev')

    def initBeamline(self):
        idx=self.UIBeamline.currentIndex()
        tag='SAR'
        if idx > 0:
            tag='SAT'
        self.plotrefBPM=None
        self.plotrefQuad=None
        self.plotrefUnd=None
        self.ModStart.clear()
        self.ModEnd.clear()
        for sec in self.BL[idx].sec:
            self.ModStart.addItem('%s%s' % (tag,sec))
            self.ModEnd.addItem('%s%s' % (tag,sec))
        self.ModStart.setEnabled(False)
        self.ModEnd.setEnabled(False)
        self.UIModRange1.setDown(True)
        self.selectRange()

    def selectRange(self):
        id=self.UIRange.checkedId()
        idx = self.UIBeamline.currentIndex()
        self.ModStart.setEnabled(False)
        self.ModEnd.setEnabled(False)
        if id == 0 :
            self.ModStart.setCurrentIndex(self.BL[idx].start)
            self.ModEnd.setCurrentIndex(self.BL[idx].end)
        elif id == 1 :
            self.ModStart.setCurrentIndex(self.BL[idx].start)
            self.ModEnd.setCurrentIndex(self.BL[idx].mid)
        elif id == 2 :
            self.ModStart.setCurrentIndex(self.BL[idx].mid+1)
            self.ModEnd.setCurrentIndex(self.BL[idx].end)
        else:
            self.ModStart.setEnabled(True)
            self.ModEnd.setEnabled(True)

    def plot(self):
        idx = self.UIBeamline.currentIndex()
        orbit = self.BL[idx].readBPM()
        orbitFB = self.BL[idx].readFB()
        self.plotrefBPM= self.plotCanvas(orbit,orbitFB,'Orbit','FB','Orbit',self.axes1,self.canvas1, self.plotrefBPM)
        
        if self.plotrefQuad is None:
            SV = self.BL[idx].readQuadSV()
            RB = self.BL[idx].readQuadRB()
            self.plotrefQuad= self.plotCanvas(SV,RB,'SV','RB','Quad Mover',self.axes2,self.canvas2, self.plotrefQuad)
        if self.plotrefUnd is None:
            SV = self.BL[idx].readUndSV()
            RB = self.BL[idx].readUndRB()
            self.plotrefUnd= self.plotCanvas(SV,RB,'SV','RB','Undulator Mover',self.axes3,self.canvas3, self.plotrefUnd)
        self.enforceMoverPlot = False

    def plotCanvas(self,y1,y2,leg1,leg2,title,axes,canvas,ref):
        lim=axes.get_ylim()
        if ref is None or np.min(y1) < lim[0] or np.min(y2) < lim[0] or np.max(y1) > lim[1] or np.max(y2) > lim[1]:
            ref = None
        if ref is None:
            idx = self.UIBeamline.currentIndex()
            lab = self.BL[idx].sec
            axes.clear()
            axes.grid()
            plt1=axes.plot(y1[0::2],'b',label=r'X-%s' % leg1)
            plt2=axes.plot(y1[1::2],'r',label=r'Y-%s' % leg1)
            plt3=axes.plot(y2[0::2],'b--',label=r'X-%s' % leg2)
            plt4=axes.plot(y2[1::2],'r--',label=r'Y-%s' % leg2)
            axes.set_xticks([i for i in range(len(lab))])
            axes.set_xticklabels(lab,rotation='vertical',size='x-small')
            axes.legend()
            axes.set_title('%s in %s' % (title,str(self.UIBeamline.currentText())))
            axes.set_ylabel(r'$x,y$ (mm)')
            axes.set_xlabel('Section')
            canvas.draw()
            return [plt1[0],plt2[0],plt3[0],plt4[0]]
        else:
            ref[0].set_ydata(y1[0::2])
            ref[1].set_ydata(y1[1::2])
            ref[2].set_ydata(y2[0::2])
            ref[3].set_ydata(y2[1::2])
        canvas.draw()
        return ref

    def enforcePlot(self):
        self.plotrefBPM = None
        self.plotrefQuad = None
        self.plotrefUnd = None

    def ToggleTimer(self):
        chk=self.UIAcRate.currentIndex();
        if chk>0:
            chk=1000/(3.3**(chk-1))
        if chk==0:
            self.timer.stop()
        else:
            self.timer.start(chk)


    def initmpl(self):
        self.fig1=Figure()
        self.axes1=self.fig1.add_subplot(111)
        self.canvas1 = FigureCanvas(self.fig1)
        self.mplvl1.addWidget(self.canvas1)
        self.canvas1.draw()

        self.fig2=Figure()
        self.axes2=self.fig2.add_subplot(111)
        self.canvas2 = FigureCanvas(self.fig2)
        self.mplvl2.addWidget(self.canvas2)
        self.canvas2.draw()

        self.fig3=Figure()
        self.axes3=self.fig3.add_subplot(111)
        self.canvas3 = FigureCanvas(self.fig3)
        self.mplvl3.addWidget(self.canvas3)
        self.canvas3.draw()

    def fillTable(self,wig,tab):
        n=len(tab)
        wig.clear()
        for i,ele in enumerate(tab):
            chkBoxItem = QtWidgets.QListWidgetItem()
            chkBoxItem.setText(ele)
            chkBoxItem.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
            chkBoxItem.setCheckState(QtCore.Qt.Checked)
            wig.addItem(chkBoxItem)

    def updateTable(self):
        self.fillTable(self.UIAramisBPM,self.BL[0].bpm)
        self.fillTable(self.UIAramisQuad,self.BL[0].qmov)
        self.fillTable(self.UIAramisUnd,self.BL[0].umov)
        self.fillTable(self.UIAthosBPM,self.BL[1].bpm)
        self.fillTable(self.UIAthosQuad,self.BL[1].qmov)
        self.fillTable(self.UIAthosUnd,self.BL[1].umov)

    def filterPV(self,item):
        sender=str(self.sender().objectName())
        val=item.checkState()
        row=self.sender().row(item)
        if val == QtCore.Qt.Checked:
            flag=1
        else:
            flag=0
        if 'Aramis' in sender:
            idx=0
        else:
            idx=1
        if 'BPM' in sender:
            self.BL[idx].bpm[row]=flag
        elif 'Quad' in sender:
            self.BL[idx].qmov[row]=flag
        elif 'Und' in sender:
            self.BL[idx].umov[row]=flag

    def modify(self):
 
        # current beamline
        idx = self.UIBeamline.currentIndex()
        # range
        istart=self.ModStart.currentIndex()
        iend=self.ModEnd.currentIndex()

        sender=str(self.sender().objectName())

        # modifying plane
        xmod=False
        if 'X' in sender:
            xmod=True

        # direction
        ref=1   # absolute setting
        if 'Dec' in sender:
            ref=-1;     # decrement

        absolute = self.UIAbsolute.isChecked()
        if absolute:
            ref = 1

        if 'Off' in sender:
            mod = 0
            amp = float(str(self.OffVal.text()))
        elif 'Slp' in sender:
            mod = 1
            amp = float(str(self.SlpVal.text()))
        else:
            mod = 2
            amp = float(str(self.ParVal.text()))

        pivit = 0.5*float(istart+iend)
        offset = [1e-3*amp*ref*(i-pivit)**mod for i in range(istart,iend+1)]

        target = self.UITarget.checkedId() # feedback = 0, quadrupole movers = 1
        doUnd = self.UIMoveUndulator.isChecked()
        compFB = self.UICompensateFB.isChecked()
        if target == 0:
            self.BL[idx].applyFB(offset,istart,iend,xmod,absolute)
        else:
            maxQuad=float(str(self.UIMaxQuad.text()))
            maxUnd=float(str(self.UIMaxUnd.text()))
            if compFB:
                self.BL[idx].applyFB([-of for of in offset],istart,iend,xmod,absolute)
            self.BL[idx].applyMover(offset,istart,iend,xmod,absolute,doUnd,maxQuad,maxUnd,self.mover)


    def capture(self):
        # current beamline
        idx = self.UIBeamline.currentIndex()
        # range
        istart=self.ModStart.currentIndex()
        iend=self.ModEnd.currentIndex()

        sender=str(self.sender().objectName())       
        # modifying plane
        xmod=False
        if 'X' in sender:
            xmod=True
        self.BL[idx].captureOrbit(istart,iend,xmod)

#  function delaing with snap shot
    def savesnap(self):
        self.snapcomment = self.snapcomment + str(self.UIBeamline.currentText())
        Thread(target=self.tsnapsave).start()
 
    def tsnapsave(self):
        status=self.snap.save(labels=[],comment=self.snapcomment)
        self.sigsnap.emit(status)

    @QtCore.pyqtSlot(bool)
    def updateSnapList(self,status):
        self.snapcomment=""
        self.UILog.setText(self.snap.message)
        self.snapsaved.append(self.snap.message.split(' to ')[1].split('/')[-1])        
        self.UISnapList.clear()
        for filename in self.snapsaved:
            self.UISnapList.addItem(filename)

    def launchsnap(self):
        if self.snappipe is None:
            self.snappipe = subprocess.Popen(['snapshot','-d',self.snap.savepath,self.snap.filename])
        else:
            if self.snappipe.poll() is not None:
                self.snappipe=None
                self.launchsnap()

    def restoresnap(self):
        curItem = self.UISnapList.currentItem()
        if curItem is None:
            return
        filename = str(curItem.text())
        idx = self.UIBeamline.currentIndex()
        refilter='SAR.*'
        if idx > 0:
            refilter='SAT.*'
        self.snap.restore(filename,refilter)

    @QtCore.pyqtSlot(int)
    def updateMoverStatus(self,stat):
        if stat == 0:
            self.UIMoverStat.setText('Idle')
            self.UIMoverStat.setStyleSheet("background-color : rgb(8,255,8);")
        elif stat == 1:
            self.UIMoverStat.setText('Moving...')
            self.UIMoverStat.setStyleSheet("background-color : yellow;")
        elif stat == 2:
            self.UIMoverStat.setText('Time Out')
            self.UIMoverStat.setStyleSheet("background-color : red;")
        elif stat == 3:
            self.UIMoverStat.setText('Abort')
            self.UIMoverStat.setStyleSheet("background-color : orange;")
        elif stat == 4:
            self.UIMoverStat.setText('Set Values are Out of Limit')
            self.UIMoverStat.setStyleSheet("background-color : red;")
        self.enforcePlot()

    def setTimeout(self):
        self.mover.timeout = float(str(self.UITimeout.text()))
        self.mover.timereset = float(str(self.UITimeReset.text()))
        self.mover.tolerance  = float(str(self.UIMotorTolerance.text()))

if __name__ == '__main__':
    QtWidgets.QApplication.setStyle(QtWidgets.QStyleFactory.create("plastique"))
    app=QtWidgets.QApplication(sys.argv)
    main=UndulatorOrbit()
    main.show()
    sys.exit(app.exec_())
