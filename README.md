# UndulatorOrbit

Program to control the orbit and pointing in the Aramis and Athos undulator beam line. It allows to change the target values of the orbit feedback or it moved the undulator and quadrupole for a physical change of the undulator beamline.

## The User Interface

![ScreenShot](rsc/UndOrbitScreen1.png)

The basic interfaces displays the orbit and the mover position for the currently selected beamline. The orbit plot is updated on a regular basis, while quadrupole and undulator mover plots are changed when the beamline is changed or the motors are driven. The major tab is `Operation` which allows for most operation tasks. It will be described in the following text. The `Expert` tab holds some extra option to control the way the orbit changes are applied.

**Beamline** Here the beamline can be chosen from the drop down menue, for which the orbit changes should be applied.

**Modification Range** This group holds a set of exclusive buttons. the current selection is indicated by a light green color. Here the operator can choose to which part the orbit changes are applied. `Full Line`, `First Half` and `Second Half` are preset ranges, indicated by the drop down menues at the right side of this group. These indicater care updated but cannot be changed unless the operator chooses the option `Custom`, which enables the pull down menues and the operator can select the range directly

**Target** This group selects whether either the feedback orbit is change or the quadrupoles are moved physically. The two buttons are exclusive. However there are two flags which can be selected. There are only applied when the quadrupoles are moved. The first `Move also Undulator` will also move the undulators with the quads as well. The other option `Compensate in FB` counter apply any quadrupole movement in the feedback target. Example: If the quadrupole is moved by 100 micron upwards the corresponding feedback target is changed by 100 microns downwards. This is needed if the operator wants to move a quadrupole without changing the current orbit. Since the BPM is moved with the quadrupole a simple move of the qusdrupole + bpm will cause an orbit bump although it will not be visible inthe orbit reading.

**Offset** This groups shifts the orbit by a constant value. The amount is given in the text box. Note that a motion in one direction is inverted if the offset value is negative (a shift by 100 micron upwards is identical to a shift by -100 micron downwards).

**Slope** The group is similar to the`Offset`, except that the offset is changed in a linear way from module to module. This changes the overall pointing of te FEL beam. The program calculates automatically the pivit point, meaning that the total amount of orbit shifts is minimal. For move flexibility apply a slope should be adjusted by additional offset (see above) if a different pivit point is needed.

**Parabolic** The group is similar to `Offset` and `Slope` but applies a quadratic change in the orbit aorund the pivit point. For a fully flexible change up to second order this option should be combined with slope and offset adjustment.

**Capture Orbit** For advanced mode, such as two color with fresh slices, the orbit deviates significantly from the standard orbit for nominal SASE operation. To simplify the set-up these buttons allow to capture the current orbit and saves it into the feedback. Note that this makes only sense if the feedback is temporarily turned off and the orbit adjusted manually.

**Reference** This group manages snapshots of the relevant machine parameters (feedback target values, quadrupole and undulator mover position) to save and restore them in case some changes should be reverted. Since the snapshot API is currently not functionally the only option is to launch the snapshot tool with the dedicated request file. Saving and restoring should be handled there.

**Advanced** This group has the flag `All changes are absolute` which enforces the program to write all setvalues in absolute terms and not relative. Example, for an offset of 100 micron, a relative change reads first the current value, then adds up offset and then writes back to the machine. When absolute setting is enabled then the assumed readback value is zero. This allows for a very fast methods to set all feedback values to zero but the flag should be used carefully. The other elements in the group is about the data acquisition rate for displaying the orbit and to enforce a refresh of the plot. The latter is needed when the automatic adjustment of the plotting range is not ideal.

**Status** This group has status meassgae. The upper is for general messages (e.g. when a snapshot file is generated) while the lower is specific for the motor control. It displays the current status, which can be idle, moving, timeout, aborted or out of range. The buttom below enforces an abort of any motion if the motor is stucked and cannot reach its desired position. Normally the timeout of the mover would quit any motion automatically.

![ScreenShot](rsc/UndOrbitScreen2.png)

**Expert Tab** This panel lists all involved actuators for the orbit control with a checkmark next to it. If an element is unchecked it will taken out of any changes, both for feedback target values or quadrupole mover position. Disabled flags have a higher precedence than the range selection on the top of the panel.  The tab has also the maximum offsets for quadrupole and undulator movers. Larger setvalues will cause an `Out of Range` messages in the status panel and disables any motion.
