import sys
import math
import numpy as np
from epics import PV


class UndulatorOrbitCore:
    def __init__(self,Aramis=True): 

        self.isAramis=Aramis
 
        # getting relevant channels
        bpmchannel=1
        if Aramis:
            # from SARMA02+SARUN01-SARUN19
            self.hasUndMov     =[0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0]
            self.enableUndMov  =[0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0]
            self.hasQuadMov    =[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
            self.enableQuadMov =[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
            self.enableFB      =[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
            self.sec=['MA02']+['UN%2.2d' %i for i in range(1,20)]
            self.dz=4.75
            self.z = [i*self.dz for i in range(20)]
            self.bpm=[]
            self.qmov=[]
            self.umov=[]
            self.start=0
            self.mid=9
            self.end=19
            for i in range(20):
                idx = 70
                if i == 0:
                    idx = 110
                self.bpm.append('SAR%s-DBPM%3.3d' % (self.sec[i],idx))
                if self.hasQuadMov[i] == 1:
                    self.qmov.append('SAR%s-MQUA%3.3d' % (self.sec[i],idx+10))
                if self.hasUndMov[i] == 1:
                    self.umov.append('SAR%s-UIND030' % (self.sec[i]))
        else:
            bpmchannel=2
           # from SATUN01-SATUN22
            self.hasUndMov    =[0,0,0,0,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1]
            self.enableUndMov =[0,0,0,0,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1]
            self.hasQuadMov   =[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
            self.enableQuadMov=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
            self.enableFB     =[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
            self.sec=['UN%2.2d' %i for i in range(1,23)]
            self.dz=2.8
            self.z = [i*self.dz for i in range(22)]
            self.bpm=[]
            self.qmov=[]
            self.umov=[]
            self.start=0
            self.mid=12
            self.end=21
            for i in range(22):
                idx = 70
                if i == 4 or i == 13:
                    idx = 410
                self.bpm.append('SAT%s-DBPM%3.3d' % (self.sec[i],idx))
                if self.hasQuadMov[i] == 1:
                    self.qmov.append('SAT%s-MQUA%3.3d' % (self.sec[i],idx+10))
                if self.hasUndMov[i] == 1:
                    self.umov.append('SAT%s-UIND030' % (self.sec[i]))
                    
        # get PVs
        self.PVbpm=[]
        self.PVFB=[]
        for pv in self.bpm:
            self.PVbpm.append(PV(pv+(':X%d-RT' % bpmchannel)))
            self.PVbpm.append(PV(pv+(':Y%d-RT' % bpmchannel)))
            self.PVFB.append(PV(pv+':X-REF-FB'))
            self.PVFB.append(PV(pv+':Y-REF-FB'))
        self.checkConnection(self.PVbpm)
        self.checkConnection(self.PVFB)

        self.PVQuadSet=[]
        self.PVQuadRead=[]
        for pv in self.qmov:
            self.PVQuadSet.append(PV(pv+(':X')))
            self.PVQuadSet.append(PV(pv+(':Y')))
            self.PVQuadRead.append(PV(pv+(':X-RBV')))
            self.PVQuadRead.append(PV(pv+(':Y-RBV')))
        self.checkConnection(self.PVQuadSet)
        self.checkConnection(self.PVQuadRead)

        self.PVUndSet=[]
        self.PVUndRead=[]
        for pv in self.umov:
            self.PVUndSet.append(PV(pv+(':GM-X-SET')))
            self.PVUndSet.append(PV(pv+(':GM-Y-SET')))
            self.PVUndRead.append(PV(pv+(':GM-X-READ')))
            self.PVUndRead.append(PV(pv+(':GM-Y-READ')))
        self.checkConnection(self.PVUndSet)
        self.checkConnection(self.PVUndRead)

        

    def checkConnection(self,pvs):
        con = [pv.wait_for_connection(timeout=0.5) for pv in pvs]
        for i,val in enumerate(con):
            if val is False:
                print('Cannot connect to PV:', pvs[i].pvname)


    def readBPM(self):
        return np.array([pv.get(timeout=0.2) for pv in self.PVbpm])

    def readFB(self):
        return np.array([pv.get(timeout=0.2) for pv in self.PVFB])
        
    def readQuadSV(self):
        return np.array([pv.get(timeout=0.2) for pv in self.PVQuadSet])

    def readQuadRB(self):
        return np.array([pv.get(timeout=0.2) for pv in self.PVQuadRead])

    def readUndSV(self):
        pvval=np.array([pv.get(timeout=0.2) for pv in self.PVUndSet])
        val=[]
        icount=0
        for i,flag in enumerate(self.hasUndMov):
            if flag is 0:
                val.append(None)
                val.append(None)
            else:
                val.append(pvval[icount])
                val.append(pvval[icount+1])
                icount=icount+2
        return np.array(val)

    def readUndRB(self):
        pvval=np.array([pv.get(timeout=0.2) for pv in self.PVUndRead])
        val=[]
        icount=0
        for i,flag in enumerate(self.hasUndMov):
            if flag is 0:
                val.append(None)
                val.append(None)
            else:
                val.append(pvval[icount])
                val.append(pvval[icount+1])
                icount=icount+2
        return np.array(val)


    def captureOrbit(self,istart,iend,xmod):
        print('Capturing orbit for feedback target values')
        fbval = self.readBPM()
        ioff=1
        if xmod:
            ioff=0
        for i in range(istart,iend+1):
            if self.enableFB[i] is 1:
                idx = i*2 + ioff
                shift = fbval[idx]
                self.PVFB[idx].put(shift)


    def applyFB(self,offset,istart,iend,xmod,absolute):
        print('Changing feedback target values')
        fbval = self.readFB()
        if absolute:
            fbval *=0
        ioff=1
        if xmod:
            ioff=0
        for i in range(istart,iend+1):
            if self.enableFB[i] is 1:
                idx = i*2 + ioff
                shift = fbval[idx]+offset[i-istart] 
                self.PVFB[idx].put(shift)
                

    def applyMover(self,offset,istart,iend,xmod,absolute,doUnd,maxQ,maxU,mover):
        print('Launching Mover Thread')
        SVQ=self.readQuadSV()
        SVU=self.readUndSV()
        if absolute:
            SVQ *= 0
            SVU *= 0
        ioff=1
        if xmod:
            ioff=0

        pvsv=[]
        pvrb=[]
        val=[]
        outofrange = False
        for i in range(istart,iend+1):
            if self.enableQuadMov[i] is 1:
                idx = i*2 + ioff
                pvsv.append(self.PVQuadSet[idx])
                val0 = SVQ[idx]+offset[i-istart]
                if np.abs(val0) > maxQ:
                    outofrange = True
                val.append(val0)
                pvrb.append(self.PVQuadRead[idx])
            if doUnd and self.enableUndMov[i] is 1: 
                for idx, pv in enumerate(self.PVUndSet):
                    if self.sec[i] in pv.pvname:
                        tag = 'GM-X'
                        if ioff == 1:
                            tag = 'GM-Y'
                        if tag in pv.pvname:
                            pvsv.append(self.PVUndSet[idx])
#                            print('Moving Undulator', pv.pvname)
                            val0 = SVU[i*2+ioff]+offset[i-istart]
                            if np.abs(val0) > maxU:
                                outofrange = True
                            val.append(val0)
                            pvrb.append(self.PVUndRead[idx])
        if outofrange:
            mover.outofrange()
            return
        else:
            mover.move(pvsv,val,pvrb)

    def getPVChannels(self):
        return self.PVFB + self.PVQuadSet + self.PVUndSet

#########################################################3
############################################################


 

